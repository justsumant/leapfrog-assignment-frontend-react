import React, { useState } from 'react';

const ContactForm = () => {


    const [nameInput, setName] = useState('');
    const [addressInput, setAddress] = useState('');
    const [contactInput, setContact] = useState('');
    const [officeContactInput, setOfficeContact] = useState('');
    const [homeContactInput, setHomeContact] = useState('');
    const [isFavoriteInput, setIsFav] = useState(false);
    const [emailInput, setEmail] = useState('');
    const [id, setId] = useState('');



    const nameHandler = (event) => {
        setName(event.target.value);
    }

    const contactHandler = (event) => {
        setContact(event.target.value);
    }

    const addressHandler = (event) => {
        setAddress(event.target.value);
    }

    const officeContactHandler = (event) => {
        setOfficeContact(event.target.value);
    }

    const homeContactHandler = (event) => {
        setHomeContact(event.target.value);
    }

    const isFavoriteHandler = (event) => {
        console.log(event.target.checked);
        setIsFav(event.target.checked);
    }

    const emailHandler = (event) => {
        setEmail(event.target.value)
    }

    const idHandler = event => {
        setId(event.target.value);
    }

    const clearForm = () => {
        setEmail('');
        setName('');
        setContact('');
        setIsFav(false);
        setHomeContact('');
        setOfficeContact('');
        setAddress('');
        setId('');
    }

    // if(isUpdateState){
    //     console.log(updatableContact);
    //     setEmail(updatableContact.email);
    //     setName(updatableContact.name);
    //     setContact(updatableContact.contactNumber);
    //     setIsFav(updatableContact.isFavorite);
    //     setHomeContact(updatableContact.homeNumber);
    //     setOfficeContact(updatableContact.officeNumber);
    //     setAddress(updatableContact.address);
    //     setId(updatableContact.id);
    // }

    const addUpdateContact = (event) => {
        event.preventDefault();
        console.log(isFavoriteInput);
        const contactData = {
            name: nameInput,
            contactNumber: contactInput,
            email: emailInput,
            homeNumber: homeContactInput,
            officeNumber: officeContactInput,
            address: addressInput,
            isFavorite: isFavoriteInput == true ? 1 : 0,
            id: id
        }

        console.log(contactData);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify(contactData)
        };

        if (isUpdateState) {
            fetch(
                'https://api.sumantgupta.com.np/updateContact', 
                // 'http://localhost:3000/updateContact',
                requestOptions)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    alert('contact updated successfully');
                    clearForm();
                }).catch(err => {
                    alert('something went wrong... ', err);
                });
        } else {
            fetch(
                'https://api.sumantgupta.com.np/addNewContact', 
                // 'http://localhost:3000/addNewContact',
                requestOptions)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    alert('contact added successfully');
                    clearForm();
                }).catch(err => {
                    alert('something went wrong... ', err);
                });
        }




    }

    return 
    <div className="card">
        <div className="card-header">
            Add New Contact
        </div>
        <div className="card-body">
            <form >
                <input type="hidden" id='id' value={id} />
                <div className="mb-3">
                    <label className="form-label" >Full Name</label>
                    <input type="text" className="form-control" value={nameInput} onChange={nameHandler} />
                    {/* <small >Please provide valid name.</small> */}
                </div>
                <div className="mb-3">
                    <label className="form-label">Email address</label>
                    <input type="email" className="form-control" value={emailInput} onChange={emailHandler} />
                    {/* <small >Please provide valid email address.</small> */}
                </div>
                <div className="mb-3">
                    <label className="form-label">Contact Number</label>
                    <input type="number" className="form-control" value={contactInput} onChange={contactHandler} />
                    {/* <small>Please provide a valid contact number.</small> */}
                </div>
                <div className="mb-3">
                    <label className="form-label">Home Contact Number</label>
                    <input type="number" className="form-control" value={homeContactInput} onChange={homeContactHandler} />
                    {/* <small>Please provide a valid contact number.</small> */}
                </div>
                <div className="mb-3">
                    <label className="form-label">Office Contact Number</label>
                    <input type="number" className="form-control" value={officeContactInput} onChange={officeContactHandler} />
                    {/* <small>Please provide a valid contact number.</small> */}
                </div>
                <div className="mb-3">
                    <label className="form-label">Address</label>
                    <input type="address" className="form-control" value={addressInput} onChange={addressHandler} />
                    {/* <small>Please provide valid address.</small> */}
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" value={isFavoriteInput} onChange={isFavoriteHandler} />
                    <label className="form-check-label">Make Favourite</label>
                </div>
                <div className="mb-3">
                    <button className="btn btn-secondary mx-2" onClick={addUpdateContact}> {isUpdateState == false ? "Add" : "Update"}</button>
                    <button type="button" className="btn btn-outline-secondary mx-2" onClick={clearForm}>Clear Form</button>
                </div>
            </form>
        </div>
    </div>
}


export default ContactForm;

