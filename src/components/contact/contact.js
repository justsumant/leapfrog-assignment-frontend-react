import React, { useState, useEffect, useCallback } from 'react';
import './contact.css';
import ConfirmationModal from '../confirmation-modal/confirmation.modal';
import Wrapper from '../helpers/wrapper';
import { useHistory } from 'react-router-dom';




const Contact = () => {

    const [contacts, setContacts] = useState([]);

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
    }
    // const baseURL = 'http://localhost:3000/';
    const baseURL = 'https://api.sumantgupta.com.np/';

    let history = useHistory();



    const fetchContacts = useCallback(async () => {
        try {
            const response = await fetch(baseURL + 'getContacts', { headers });
            if (!response.ok) {
                history.push("/login");
            }
            setContacts(await response.json());
        } catch (error) {
            console.log(error);
        }
    }, []);


    useEffect(() => {
        fetchContacts();

    }, [fetchContacts]);


    const deleteContactHandler = (id) => {
        console.log('going to delete contact with id : ' + id);
        const requestOptions = {
            method: 'DELETE',
            headers: headers
        };

        fetch(
            baseURL + 'deleteContact' + id,
            requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                fetchContacts();
            });
    }

    const toggleFavorite = (contact) => {
        contact.isFavorite = !contact.isFavorite;
        const requestOptions = {
            method: 'POST',
            headers: headers
        };

        fetch(
            baseURL + 'updateContact',
            requestOptions,
            JSON.stringify(contact)
        )
            .then(response => response.json())
            .then(data => {
                console.log(data);
                fetchContacts();
            });
    }


    return
    <Wrapper>
        <h4>
            All Contacts
        </h4>
        <div className="fixed-box">
            {contacts.length === 0 ? (<p>No contact found, try to add some first.</p>) : (
                contacts.map((contact) => (
                    <div className="card my-3" key={contact.id}>
                        <div className="card-header">
                            <div className="d-flex justify-content-between">
                                <div><b>{contact.name}</b>, {contact.address}</div>
                                <div>
                                    {contact.isFavorite == 0 ? (<button className='starButton' onClick={() => toggleFavorite(contact)}><i className="far fa-star" ></i></button>) : (<button className='starButton' onClick={() => toggleFavorite(contact)}><i className="fas fa-star" ></i></button>)}
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-10">
                                    <div className="d-flex justify-content-between">
                                        <h6 ><a href={"tel:" + contact.contactNumber}><i className="fas fa-mobile-alt"></i> {contact.contactNumber}</a>
                                        </h6>
                                        <h6 ><a href={"tel:" + contact.homeNumber}><i className="fas fa-home"></i> {contact.homeNumber}</a>
                                        </h6>
                                        <h6 ><a href={"tel:" + contact.officeNumber}><i className="fas fa-building"></i> {contact.officeNumber}</a>
                                        </h6>
                                    </div>
                                    <div>
                                        <h6> <a href={"mailto:" + contact.email}><i className="fas fa-envelope"></i> {contact.email}</a>
                                        </h6>
                                    </div>
                                </div>
                                <div className="col-2">
                                    <button type="button" className="btn btn-danger btn-sm m-1"
                                        onClick={() => deleteContactHandler(contact.id)}
                                    ><i className="fas fa-trash"></i></button>
                                    <button type="button" className="btn btn-secondary btn-sm m-1" 
                                    ><i className="fas fa-edit"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            )}
        </div>
    </Wrapper>

}

export default Contact;
